//
//  MainViewController.swift
//  MyProject
//
//  Created by CHINNA on 15/09/18.
//  Copyright © 2018 CHINNA. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ModelData
{
    var name : String?
    var Description:String?
    var ProfileImage:String?
    init(name:String?,Desctiption:String,ProfileImage:String) {
        self.name = name
        self.Description = Desctiption
        self.ProfileImage = ProfileImage
    }
}


class MainViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating,UISearchControllerDelegate,UISearchBarDelegate  {
    
    
    @IBOutlet weak var TableView: UITableView!
    
    var modeldata = [ModelData]()
    var FilerData = [ModelData]()
    let rect = CGRect(x: 0, y: 0, width: 400, height: 0)
    
    var searchcontroller : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchcontroller = UISearchController(searchResultsController: nil)
        self.searchcontroller.delegate = self
        self.searchcontroller.searchBar.delegate = self
        self.searchcontroller.hidesNavigationBarDuringPresentation = false
        self.searchcontroller.dimsBackgroundDuringPresentation = false
        self.navigationItem.titleView = searchcontroller.searchBar
        self.searchcontroller.searchResultsUpdater = self
        jsonparse()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchcontroller.searchBar.text = ""
        self.reloaddata()
        
    }
    
    func jsonparse()
    {
        
        let url = URL(string: "https://en.wikipedia.org//w/api.php?action=query&format=json&prop=pageimages%7Cpageterms&generator=prefixsearch&redirects=1&formatversion=2&piprop=thumbnail&pithumbsize=50&pilimit=10&wbptterms=description&gpssearch=rahul&gpslimit=10")
        
        
        Alamofire.request(url!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: {
            response in
            guard let jsondata = response.result.value as? [String:Any] else{return }
            guard let querydata = jsondata["query"] as? [String:Any] else {return}
            guard let Pages = querydata["pages"] as? [[String:AnyObject]] else { return }
            
            for PagesData in Pages
            {
                let title = PagesData["title"] as! String
                var profileImage:String = ""
                if let image = (PagesData["thumbnail"] as?[String:Any])
                {
                    profileImage = image["source"] as! String
                    
                }
                else {
                    profileImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Shiva_Temple%2C_Dhoni%2C_Palakkad.jpg/37px-Shiva_Temple%2C_Dhoni%2C_Palakkad.jpg"
                }
                
                var descrption:String = ""
                if let term = (PagesData["terms"] as? [String:Any])
                {
                    let desc = term["description"] as? NSArray
                    descrption = desc?.firstObject as! String
                }
                else {
                    descrption = "descdouble-headed Indian drum"
                }
                print("title \(title) profile \(profileImage) desc\(descrption)")
                
                self.modeldata.append(ModelData.init(name: title, Desctiption: descrption, ProfileImage: profileImage))
            }
            self.reloaddata()
            self.FilerData = self.modeldata
        }).resume()
        
    }
    
    func reloaddata()
    {
        DispatchQueue.main.async {
            self.TableView.reloadData()
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modeldata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell
        let row = indexPath.row
        let values = modeldata[row] as ModelData
        
        cell.NameLabel.text = values.name
        cell.DescLabel.text = values.Description
        
        let url = URL(string: values.ProfileImage!)
        cell.Profile.af_setImage(withURL: url!)
        let radius = cell.Profile.frame.width/2
        cell.Profile.layer.cornerRadius = radius
        cell.Profile.clipsToBounds = true
        return cell
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchToSearch = searchController.searchBar.text
        if(searchToSearch == "")
        {
            modeldata = self.FilerData
        }
        else{
            modeldata.removeAll()
            let itemsarray = self.FilerData
            var ListArray = [String]()
            for ListItems in itemsarray {
                ListArray.append(ListItems.name!)
                if(ListItems.name?.range(of: searchToSearch!, options: .caseInsensitive) != nil)
                {
                    self.modeldata.append(ListItems)
                    
                }
            }
        }
        self.TableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WK") as? WekiViewController
        let index = indexPath.row
        let name = modeldata[index].name
        vc?.namestring = name!
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
}
