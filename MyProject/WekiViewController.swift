//
//  WekiViewController.swift
//  MyProject
//
//  Created by CHINNA on 15/09/18.
//  Copyright © 2018 CHINNA. All rights reserved.
//

import UIKit

class WekiViewController: UIViewController {

    var namestring :String = ""
   
    
    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
    super.viewDidLoad()

        
      //  https://en.wikipedia.org/wiki/Chicago
        let converstring = namestring.replacingOccurrences(of: " ", with: "_")
        let url = URL(string: "https://en.wikipedia.org/wiki/" + converstring)
        var requesobj = URLRequest(url: url!)
        requesobj.cachePolicy = .returnCacheDataElseLoad
        webview.loadRequest(requesobj)
        
        print("stringvalues ",namestring as Any)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
